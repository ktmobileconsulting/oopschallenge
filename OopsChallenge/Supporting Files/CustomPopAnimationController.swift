//
//  CustomPopAnimationController.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import UIKit

class CustomPopAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from),
              let toVC = transitionContext.viewController(forKey: .to),
              let toView = transitionContext.view(forKey: .to) else {
            return
        }

        let containerView = transitionContext.containerView
        containerView.insertSubview(toView, belowSubview: fromVC.view)
        
        let initialFrame = transitionContext.initialFrame(for: fromVC)
        toView.frame = initialFrame.offsetBy(dx: -initialFrame.width * 0.3, dy: 0)
        fromVC.view.frame = initialFrame

        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            fromVC.view.frame = initialFrame.offsetBy(dx: initialFrame.width, dy: 0)
            toView.frame = initialFrame
        }, completion: { _ in
            if !transitionContext.transitionWasCancelled {
                toVC.navigationController?.setNavigationBarHidden(false, animated: false)
            }
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}

