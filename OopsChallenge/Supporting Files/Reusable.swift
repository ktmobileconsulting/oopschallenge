//
//  Reusable.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import UIKit

public protocol ReusableView: AnyObject {
    static var defaultReuseIdentifier: String { get }
}

public extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return NSStringFromClass(self).components(separatedBy: ".").last ?? ""
    }

    static var defaultNibName: String {
        return NSStringFromClass(self).components(separatedBy: ".").last ?? ""
    }
}

extension UICollectionViewCell: ReusableView {
}

public extension UICollectionView {
    func registerClass<CellClass: UICollectionViewCell>(_ cellClass: CellClass.Type) {
        register(CellClass.self, forCellWithReuseIdentifier: cellClass.defaultReuseIdentifier)
    }

    func registerNib<CellClass: UICollectionViewCell>(_ cellClass: CellClass.Type) {
        let bundle = Bundle(for: CellClass.self)
        let nib = UINib(nibName: cellClass.defaultNibName, bundle: bundle)
        register(nib, forCellWithReuseIdentifier: cellClass.defaultReuseIdentifier)
    }

    func dequeueReusableCell<CellClass: UICollectionViewCell>(_ cellClass: CellClass.Type, for indexPath: IndexPath) -> CellClass {
        return dequeueReusableCell(withReuseIdentifier: cellClass.defaultReuseIdentifier, for: indexPath) as? CellClass ?? CellClass()
    }
}

extension UILabel {
    func setAttributedText(text: String, font: UIFont, color: UIColor) {
        let valueAttributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: color]
        let attributedString = NSMutableAttributedString(string: text, attributes: valueAttributes)
        
        self.attributedText = attributedString
    }
}
