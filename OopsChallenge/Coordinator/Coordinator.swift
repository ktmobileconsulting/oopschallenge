//
//  Coordinator.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import UIKit

protocol Coordinator {
    var sourceNavigationController: UINavigationController { get set }

    func start()
}
