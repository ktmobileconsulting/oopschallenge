//
//  MainCoordinator.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import UIKit

class MainCoordinator: Coordinator {
    
    // MARK: - Properties

    var sourceNavigationController: UINavigationController
    
    // MARK: - Initalization

    init(sourceNavigationController: UINavigationController) {
        self.sourceNavigationController = sourceNavigationController
    }
    
    // MARK: - Functions

    func start() {
        let mainCollectionViewController = MainViewController(collectionViewLayout: UICollectionViewFlowLayout())
        mainCollectionViewController.delegate = self
        sourceNavigationController.pushViewController(mainCollectionViewController, animated: true)
    }
}

extension MainCoordinator: MainViewControllerDelegate {
    func showDetails(_ film: Film) {
        let detailsViewController = DetailsViewController(film: film)
        sourceNavigationController.isNavigationBarHidden = true
        sourceNavigationController.pushViewController(detailsViewController, animated: true)
    }

    func addFilm() {
        let addViewController = AddViewController()
        let navController = UINavigationController(rootViewController: addViewController)
        sourceNavigationController.present(navController, animated: true)
    }
}
