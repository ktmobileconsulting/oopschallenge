//
//  MainViewModel.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import UIKit

class MainViewModel {
    var allFilms: [Film] = [ Film(image: UIImage(named: "poster1") ?? UIImage(),
                                  backgroundImage: UIImage(named: "bg1") ?? UIImage(),
                                  gradientColors: [UIColor(red: 0.93, green: 0.84, blue: 0.84, alpha: 1).cgColor,
                                                   UIColor(red: 0.56, green: 0.27, blue: 0.29, alpha: 1).cgColor],
                                  imageName: UIImage(named: "name1") ?? UIImage(),
                                  infologos: nil,
                                  description: "An aging Chinese immigrant is swept up in an insane adventure, where she alone can save what’s important to her by connecting with the lives she could have led in other universes."),
                             Film(image: UIImage(named: "poster2") ?? UIImage(),
                                  backgroundImage: UIImage(named: "bg2") ?? UIImage(),
                                  gradientColors: [UIColor(red: 0.32, green: 0.34, blue: 0.38, alpha: 1).cgColor,
                                                   UIColor(red: 0.54, green: 0.54, blue: 0.54, alpha: 1).cgColor],
                                  imageName: UIImage(named: "name2") ?? UIImage(),
                                  infologos: nil,
                                  description: "Caesar and his apes are forced into a deadly conflict with an army of humans led by a ruthless Colonel. After the apes suffer unimaginable losses, Caesar wrestles with his darker instincts and begins his own mythic quest to avenge his kind. As the journey finally brings them face to face, Caesar and the Colonel are pitted against each other in an epic battle that will determine the fate of both their species and the future of the planet.")
    ]
}
