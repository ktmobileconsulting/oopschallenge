//
//  Film.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import UIKit

struct Film {
    let image: UIImage
    let backgroundImage: UIImage
    let gradientColors: [CGColor]
    let imageName: UIImage
    let infologos: [UIImage]?
    let description: String
}
