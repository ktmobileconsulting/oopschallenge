//
//  AddViewController.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import UIKit

class AddViewController: UIViewController {
    
    // MARK: - Private properties
    
    private lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 16
        stackView.alignment = .fill
        stackView.axis = .vertical
        return stackView
    }()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        setup()
    }

    // MARK: - Actions

    @objc func leftBarButtonTapped() {
        dismiss(animated: true)
    }

    @objc func rightBarButtonTapped() {
        print("add")
    }

    // MARK: - Initial Setup

    private func setup() {
        configureNavBar()
        configureButtons()
        
        configureViews()
    }

    // MARK: - Private functions

    private func configureNavBar() {
        self.title = "Add a Movie"

        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        self.navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.black]

        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .automatic
    }

    private func configureButtons() {
        let leftBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(leftBarButtonTapped))
        self.navigationItem.leftBarButtonItem = leftBarButton

        let rightBarButton = UIBarButtonItem(title: "Add", style: .done, target: self, action: #selector(rightBarButtonTapped))
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    private func configureViews() {
        view.addSubview(contentStackView)

        contentStackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 9).isActive = true
        contentStackView.autoPinEdge(toSuperviewEdge: .left, withInset: 25)
        contentStackView.autoPinEdge(toSuperviewEdge: .right, withInset: 25)

        for _ in 0..<3 {
            let view = UIView()
            view.layer.cornerRadius = 20
            view.backgroundColor = UIColor(red: 0.953, green: 0.953, blue: 0.953, alpha: 1)
            view.autoSetDimension(.height, toSize: 172)
            contentStackView.addArrangedSubview(view)
        }
    }
}
