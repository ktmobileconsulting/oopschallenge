//
//  DetailsViewController.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import UIKit
import PureLayout

protocol DetailsViewControllerDelegate: AnyObject {
    func dismiss()
}

class DetailsViewController: UIViewController {
    
    // MARK: - Properties

    var interactiveTransition: UIPercentDrivenInteractiveTransition?
    weak var delegate: DetailsViewControllerDelegate?

    // MARK: - Private properties

    private lazy var contentView = DetailsContentView(film: film)

    private let film: Film

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.delegate = self

        setup()
    }

    init(film: Film) {
        self.film = film

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Internal Setup
    
    private func setup() {
        setupCustomSwipe()

        setupBackgroundGradient()
        view.addSubview(contentView)
        contentView.delegate = self
        contentView.autoPinEdgesToSuperviewEdges()
    }
    
    // MARK: - Actions

    @objc private func handleSwipe(_ gesture: UIScreenEdgePanGestureRecognizer) {
        let progress = gesture.translation(in: view).x / view.bounds.width
        
        switch gesture.state {
        case .began:
            interactiveTransition = UIPercentDrivenInteractiveTransition()
            navigationController?.popViewController(animated: true)
        case .changed:
            interactiveTransition?.update(progress)
        case .ended, .cancelled:
            if progress > 0.5 {
                interactiveTransition?.finish()
            } else {
                interactiveTransition?.cancel()
            }
            interactiveTransition = nil
        default:
            break
        }
    }

    // MARK: - Private functions
    
    private func setupCustomSwipe() {
        let swipeGesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(handleSwipe(_:)))
        swipeGesture.edges = .left
        view.addGestureRecognizer(swipeGesture)
    }

    private func setupBackgroundGradient() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = film.gradientColors
        gradientLayer.locations = [0, 1]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
        gradientLayer.frame = view.bounds
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
}
