//
//  DetailsViewController+Extension.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import UIKit

extension DetailsViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactiveTransition
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == .pop {
            return CustomPopAnimationController()
        }
        return nil
    }
}

extension DetailsViewController: DetailsContentViewDelegate {
    func shareButtonDidTapped(_ image: UIImage) {
        let text = "Share or Save this poster"
        let url = URL(string: "https://apps.apple.com/us/app/oops/id1593466495")
        let activityItems = [text, image, url!] as [Any]

        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        
        activityViewController.completionWithItemsHandler = { [weak self] (activityType, completed, items, error) in
            if completed {
                self?.showAlert(withTitle: "Done!", message: "The action has been completed.")
            } else if let error = error {
                self?.showAlert(withTitle: "Error", message: error.localizedDescription)
            } else {
                self?.showAlert(withTitle: "Cancelled", message: "You cancelled the action.")
            }
        }

        present(activityViewController, animated: true, completion: nil)
    }
    
    private func showAlert(withTitle title: String, message: String) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
}
