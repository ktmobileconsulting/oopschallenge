//
//  DetailsContentView.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import UIKit

protocol DetailsContentViewDelegate: AnyObject {
    func shareButtonDidTapped(_ image: UIImage)
}

class DetailsContentView: UIView {
    
    weak var delegate: DetailsContentViewDelegate?

    // MARK: - Private properties

    private let scrollView = UIScrollView()
    private let contentView = UIView()

    private let backgroundImage = UIImageView()
    private let imageView = UIImageView()
    private let titleView = UIImageView()
    private let descriptionLabel = UILabel()
    
    private let shareButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(red: 0.37, green: 0.37, blue: 0.37, alpha: 0.5)
        button.layer.cornerRadius = 33
        button.autoSetDimensions(to: CGSize(width: 66, height: 66))

        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        button.layer.shadowRadius = 4
        button.layer.shadowOpacity = 0.5
        button.layer.masksToBounds = false

        let configuration = UIImage.SymbolConfiguration(pointSize: 24, weight: .bold)
        if let shareImage = UIImage(systemName: "square.and.arrow.up",withConfiguration: configuration) {
            button.setImage(shareImage, for: .normal)
            button.tintColor = .white
        }

        return button
    }()


    private let film: Film

    // MARK: - Construction

    init(film: Film) {
        self.film = film
        super.init(frame: .zero)
        
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func didTapShareButton() {
        guard let image = captureContentAsImage() else {
            return
        }

        delegate?.shareButtonDidTapped(image)
    }

    private func captureContentAsImage() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(contentView.bounds.size, contentView.isOpaque, 0.0)
        defer { UIGraphicsEndImageContext() }
        contentView.drawHierarchy(in: contentView.bounds, afterScreenUpdates: true)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    private func setup() {
        setupbgImageView()

        setupScrollView()
        setupContentView()

        setupImageView()
        setupTitleView()
        setupDescriptionLabel()
        
        setupShareButton()
    }
    
    private func setupScrollView() {
        addSubview(scrollView)
        scrollView.autoPinEdgesToSuperviewEdges()
        scrollView.contentInsetAdjustmentBehavior = .never

        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
    }

    private func setupContentView() {
        scrollView.addSubview(contentView)
        contentView.backgroundColor = .clear
        contentView.autoPinEdgesToSuperviewEdges()
        contentView.autoMatch(.width, to: .width, of: scrollView)
    }
    
    private func setupbgImageView() {
        contentView.addSubview(backgroundImage)
        backgroundImage.image = film.backgroundImage
        backgroundImage.contentMode = .scaleAspectFill
        backgroundImage.alpha = 0.3
        backgroundImage.clipsToBounds = true

        backgroundImage.autoPinEdge(toSuperviewEdge: .top)
        backgroundImage.autoPinEdge(toSuperviewEdge: .left)
        backgroundImage.autoPinEdge(toSuperviewEdge: .right)
        backgroundImage.autoSetDimension(.height, toSize: 560)
    }
    
    private func setupImageView() {
        contentView.addSubview(imageView)
        imageView.image = film.image
        imageView.layer.cornerRadius = 20
        imageView.clipsToBounds = true
        
        imageView.autoPinEdge(toSuperviewEdge: .top, withInset: 75)
        imageView.autoPinEdge(toSuperviewEdge: .left, withInset: 62)
        imageView.autoPinEdge(toSuperviewEdge: .right, withInset: 62)
        imageView.autoSetDimension(.height, toSize: 390)
    }

    private func setupTitleView() {
        contentView.addSubview(titleView)
        titleView.image = film.imageName

        titleView.autoPinEdge(.top, to: .bottom, of: imageView, withOffset: 36)
        titleView.autoPinEdge(toSuperviewEdge: .left, withInset: 62)
        titleView.autoPinEdge(toSuperviewEdge: .right, withInset: 62)
        titleView.autoSetDimension(.height, toSize: 124)
    }

    private func setupDescriptionLabel() {
        contentView.addSubview(descriptionLabel)
        descriptionLabel.setAttributedText(text: film.description, font: UIFont.systemFont(ofSize: 17, weight: .medium), color: UIColor.white)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .center

        descriptionLabel.autoPinEdge(.top, to: .bottom, of: titleView, withOffset: 36)
        descriptionLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 30)
        descriptionLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 30)
        descriptionLabel.autoPinEdge(toSuperviewEdge: .bottom, withInset: 90)
    }
    
    private func setupShareButton() {
        addSubview(shareButton)
        shareButton.autoPinEdge(toSuperviewEdge: .right, withInset: 20)
        shareButton.autoPinEdge(toSuperviewEdge: .bottom, withInset: 30)

        shareButton.addTarget(self, action: #selector(didTapShareButton), for: .touchUpInside)
    }
}
