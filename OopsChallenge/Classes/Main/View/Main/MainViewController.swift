//
//  MainViewController.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import UIKit

protocol MainViewControllerDelegate: AnyObject {
    func addFilm()
    func showDetails(_ film: Film)
}

class MainViewController: UICollectionViewController {
    
    // MARK: - Properties
    
    weak var delegate: MainViewControllerDelegate?

    // MARK: - Private properties
    
    private let viewModel = MainViewModel()
    private lazy var data: [Film] = viewModel.allFilms

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
    
    // MARK: - Initial Setup

    private func setup() {
        configureNavBar()
        configureRightButton()

        configureCollection()
    }

    // MARK: - Private proreties

    private func configureNavBar() {
        self.title = "Library"

        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        self.navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.black]

        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .automatic
        
    }

    private func configureRightButton() {
        let rightBarButton = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(rightBarButtonTapped))
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    private func configureCollection() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 22, left: 20, bottom: 22, right: 10)
        layout.minimumInteritemSpacing = 14
        layout.minimumLineSpacing = 5
        layout.itemSize = CGSize(width: 110, height: 205)

        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .white

        collectionView.registerClass(MainCollectionViewCell.self)
    }
    
    // MARK: - Actions

    @objc func rightBarButtonTapped() {
        delegate?.addFilm()
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(MainCollectionViewCell.self, for: indexPath)

        cell.update(with: data[indexPath.item].image)

        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.showDetails(data[indexPath.row])
    }
}
