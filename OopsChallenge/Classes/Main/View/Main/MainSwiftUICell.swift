//
//  MainSwiftUICell.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import SwiftUI

struct MySwiftUIView: View {
    var image: UIImage

    var body: some View {
        ZStack {
            backgroundRectangle
                .offset(x: -5, y: -3)
            imageRectangle
        }
        .padding(EdgeInsets(top: 3, leading: 4, bottom: 3, trailing: 4))
    }
    
    private var backgroundRectangle: some View {
        Rectangle()
            .foregroundColor(.clear)
            .frame(width: 105, height: 186)
            .background(
                LinearGradient(
                    gradient: Gradient(colors: [Color.red, Color.blue]),
                    startPoint: UnitPoint(x: 0.5, y: 0),
                    endPoint: UnitPoint(x: 0.5, y: 1)
                )
            )
            .cornerRadius(15)
            .rotation3DEffect(.degrees(8), axis: (x: 0, y: 1, z: 0))
    }
    
    private var imageRectangle: some View {
        Rectangle()
            .foregroundColor(.clear)
            .frame(width: 105, height: 186)
            .background(
                Image(uiImage: image)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 105, height: 186)
                    .clipped()
            )
            .cornerRadius(12)
            .shadow(color: .black.opacity(0.3), radius: 7, x: 0, y: 8)
            .rotation3DEffect(.degrees(8), axis: (x: 0, y: 1, z: 0))
    }
}
