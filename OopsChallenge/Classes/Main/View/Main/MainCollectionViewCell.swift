//
//  MainCollectionViewCell.swift
//  OopsChallenge
//
//  Created by Kirill Trokhov on 8/11/23.
//

import Foundation
import UIKit
import SwiftUI
import PureLayout

class MainCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Private properties

    private var hostingController: UIHostingController<AnyView>?

    // MARK:  - Initiaziation

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupHostingController()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupHostingController()
    }
    
    // MARK: - Functions
    
    func update(with imageFilm: UIImage) {
        let swiftUIView = MySwiftUIView(image: imageFilm)
        hostingController?.rootView = AnyView(swiftUIView)
        hostingController?.view.setNeedsDisplay()
    }
    
    // MARK: - Private functions

    private func setupHostingController() {
        hostingController = UIHostingController(rootView: AnyView(EmptyView()))
        guard let view = hostingController?.view else { return }
        view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(view)
        view.backgroundColor = .clear
        view.autoPinEdgesToSuperviewEdges()
    }
}
